<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserManagementController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DetailController;
use App\Http\Controllers\KeranjangController;
use App\Http\Controllers\PesananController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\TransaksiDetailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// homepage
Route::get('/', [HomeController::class,'homepage'])->name('homepage');

// login
Route::get('/login', [LoginController::class,'index'])->name('login');
Route::post('/login/proses', [LoginController::class,'login'])->name('login.proses');

//logout
Route::get('/logout', [LoginController::class,'logout'])->name('logout');

// register
Route::get('/daftar', [LoginController::class,'daftar'])->name('daftar');
Route::post('/daftar/proses', [LoginController::class,'daftar_proses'])->name('daftar.proses');

Route::middleware(['auth'])->group(function(){

    // admin
    Route::get('/admin/dashboard', [AdminController::class,'index'])->name('dashboard.admin')->middleware('user.akses:admin');

    // tabel user
    Route::get('/admin/user/index', [UserManagementController::class, 'index'])->name('user.index')->middleware('user.akses:admin');
    Route::get('/admin/user/create', [UserManagementController::class, 'create'])->name('user.create')->middleware('user.akses:admin');
    Route::post('/admin/user/store', [UserManagementController::class, 'store'])->name('user.store')->middleware('user.akses:admin');
    Route::get('/admin/user/{id}/edit', [UserManagementController::class, 'edit'])->name('user.edit')->middleware('user.akses:admin');
    Route::put('/admin/user/update/{id}', [UserManagementController::class, 'update'])->name('user.update')->middleware('user.akses:admin');
    Route::delete('/admin/user/delete/{id}', [UserManagementController::class, 'delete'])->name('user.delete')->middleware('user.akses:admin');

    // tabel produk
    Route::get('/admin/produk/index', [ProdukController::class, 'index'])->name('produk.index')->middleware('user.akses:admin');
    Route::get('/admin/produk/create', [ProdukController::class, 'create'])->name('produk.create')->middleware('user.akses:admin');
    Route::post('/admin/produk/store', [ProdukController::class, 'store'])->name('produk.store')->middleware('user.akses:admin');
    Route::get('/admin/produk/{id}/edit', [ProdukController::class, 'edit'])->name('produk.edit')->middleware('user.akses:admin');
    Route::put('/admin/produk/update/{id}', [ProdukController::class, 'update'])->name('produk.update')->middleware('user.akses:admin');
    Route::delete('/admin/produk/delete/{id}', [ProdukController::class, 'delete'])->name('produk.delete')->middleware('user.akses:admin');

    // tabel pesanan
    Route::get('/admin/pesanan/index', [TransaksiController::class, 'index'])->name('pesanan.index')->middleware('user.akses:admin');
    Route::delete('/admin/transaksi/delete/{id}', [TransaksiController::class, 'delete'])->name('transaksi.delete')->middleware('user.akses:admin');


    // tabel detail
    Route::get('/admin/transaksi/detail/{id}', [TransaksiController::class, 'show'])->name('transaksi.detail')->middleware('user.akses:admin');
    // Route::get('/admin/detail/index', [TransaksiDetailController::class, 'index'])->name('detail.index')->middleware('user.akses:admin');

    //user
    Route::get('/user', [UserController::class, 'index'])->name('dashboard.user')->middleware('user.akses:user');
    Route::get('/menu', [UserController::class, 'menu'])->name('menu')->middleware('user.akses:user');
    // Route::get('/keranjang', [UserController::class, 'keranjang'])->name('keranjang')->middleware('user.akses:user');
    Route::get('/pesanan', [UserController::class, 'pesanan'])->name('pesanan')->middleware('user.akses:user');

    //keranjang
    Route::get('/keranjang', [KeranjangController::class, 'index'])->name('keranjang')->middleware('user.akses:user');
    Route::post('/keranjang/{id}', [KeranjangController::class, 'store'])->name('keranjang.store')->middleware('user.akses:user');
    Route::patch('/keranjang/update/{id}', [KeranjangController::class, 'update'])->name('keranjang.update')->middleware('user.akses:user');
    Route::delete('/keranjang/delete/{id}', [KeranjangController::class, 'destroy'])->name('keranjang.destroy')->middleware('user.akses:user');

    // transaksi
    Route::post('/konfirmasi', [TransaksiController::class, 'store'])->name('konfirmasi')->middleware('user.akses:user');
    // Route::get('/pesanan', [TransaksiController::class, 'index'])->name('pesanan')->middleware('user.akses:user');

    // detail
    Route::get('/transaksi/detail/{id}', [TransaksiDetailController::class, 'show'])->name('detail')->middleware('user.akses:user');
});





