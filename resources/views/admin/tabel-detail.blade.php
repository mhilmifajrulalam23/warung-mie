@extends('admin.layout.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Detail Pesanan</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            {{-- <div class="card-header"> --}}
                            {{-- <h3 class="card-title">Tabel User</h3> --}}
                            {{-- <a href="{{route('user.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a> --}}

                            {{-- <div class="card-tools"> --}}
                            {{-- <div class="input-group input-group-sm" style="width: 150px;"> --}}
                            {{-- <input type="text" name="table_search" class="form-control float-right"
                                                placeholder="Search"> --}}

                            {{-- <div class="input-group-append"> --}}
                            {{-- <button type="submit" class="btn btn-default"> --}}
                            {{-- <i class="fas fa-search"></i> --}}
                            {{-- </button> --}}
                            {{-- </div> --}}
                            {{-- </div> --}}
                            {{-- </div> --}}
                            {{-- </div> --}}
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            {{-- <th>Id Transaksi</th> --}}
                                            <th>produk</th>
                                            <th>qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($transaksi_details as $d)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                {{-- <td>{{ $d->id_transaksis }}</td> --}}
                                                <td>{{ $d->produk->nama_produk }}</td>
                                                <td>{{ $d->qty }}</td>
                                                <td>{{ $d->jumlah }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
