  @extends('admin.layout.main')
  @section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          <!-- Content Header (Page header) -->

          <div class="content-header">
              <div class="container-fluid">
                  <div class="row mb-2">
                      <div class="col-sm-6">
                          <h1 class="m-0">Dashboard</h1>
                      </div><!-- /.col -->
                  </div><!-- /.row -->
              </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->

          <!-- Main content -->

          <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box">
                          <span class="info-box-icon bg-success"><i class="fa-regular fa-user"></i></span>

                          <div class="info-box-content">
                            <span class="info-box-text">User</span>
                            <span class="info-box-number">{{ $totalUser }}</span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                      </div>
                    <div class="col-md-3 col-sm-6 col-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa-solid fa-utensils"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text">Produk</span>
                          <span class="info-box-number">{{ $totalProduk }}</span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-warning"><i class="fa-solid fa-copy"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text">Pesanan</span>
                          <span class="info-box-number">{{ $totalTransaksi }}</span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa-solid fa-money-bill"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text">Transaksi</span>
                          <span class="info-box-number"> @currency($totalUangMasuk)</span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                  </div>
          </section>
          <!-- /.content -->
      </div>
  @endsection
