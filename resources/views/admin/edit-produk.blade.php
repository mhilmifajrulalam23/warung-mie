@extends('admin.layout.main')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Data Produk</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form action="{{route('produk.update',['id'=>$produk->id])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Form edit produk</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleInputName">Nama Produk</label>
                                            <input type="text" value="{{$produk->nama_produk}}" name="nama_produk" class="form-control" id="exampleInputName"
                                                placeholder="Masukkan Nama">
                                            @error('nama_produk')
                                                {{ $message }}
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName">Deskripsi</label>
                                            <input type="text" value="{{$produk->deskripsi}}" name="deskripsi" class="form-control"
                                                id="exampleInputName" placeholder="Masukkan Deskripsi">
                                            @error('deskripsi')
                                                {{ $message }}
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputRole">Role</label>
                                            <select name="jenis" class="form-control">
                                                <option value="{{$produk->jenis}}" hidden>{{$produk->jenis}}</option>
                                                <option value="makanan">makanan</option>
                                                <option value="minuman">minuman</option>
                                            </select>
                                            @error('jenis')
                                                {{ $message }}
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName">Harga</label>
                                            <input type="text" value="{{$produk->harga}}" name="harga" class="form-control" id="exampleInputName"
                                                placeholder="Masukkan Harga">
                                            @error('harga')
                                                {{ $message }}
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">Gambar</label>
                                            <input type="file" value="{{$produk->gambar}}" name="gambar" class="form-control"
                                                id="exampleInputFile">
                                            @error('gambar')
                                                {{ $message }}
                                            @enderror
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="exampleInputFile">Gambar</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" name="gambar" class="custom-file-input" id="exampleInputFile">
                                                    <label class="custom-file-label" for="exampleInputFile">Pilih Gambar</label>
                                                </div>
                                            </div>
                                            @error('gambar')
                                                {{ $message }}
                                            @enderror
                                        </div> --}}
                                        <!-- /.card-body -->
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary">Edit</button>
                                        </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (right) -->
                    </div>
                </form>

                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
