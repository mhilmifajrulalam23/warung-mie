@extends('user.layout.main')
@section('content')
    <div class="container mt-6">
        <h2 class="mt-5">Makanan</h2>
        <div class="row">
            @foreach ($produk as $p)
                @if ($p->jenis == 'makanan')
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4">
                        <div class="card" style="width: 100;">
                            <img src="{{ asset('storage/gambar-produk/' . $p->gambar) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{ $p->nama_produk }}</h5>
                                <p class="card-text">{{ $p->deskripsi }}</p>
                                <h6 class="card-text">@currency($p->harga)</h6>
                                <form action="{{route('keranjang.store', ['id' => $p->id])}}" method="POST">
                                    @csrf
                                    <input type="hidden" value="{{$p->id}}" name="id_produks">
                                    <input type="submit" class="btn btn-custom" value="pesan">
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        <h2>Minuman</h2>
        <div class="row">
            @foreach ($produk as $p)
                @if ($p->jenis == 'minuman')
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4">
                        <div class="card" style="width: 100;">
                            <img src="{{ asset('storage/gambar-produk/' . $p->gambar) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{ $p->nama_produk }}</h5>
                                <p class="card-text">{{ $p->deskripsi }}</p>
                                <h6 class="card-text">@currency($p->harga)</h6>
                                <form action="{{route('keranjang.store', ['id' => $p->id])}}" method="POST">
                                    @csrf
                                    <input type="hidden" value="{{$p->id}}" name="id_produks">
                                    <button type="submit" class="btn btn-custom">Pesan</button>
                                    {{-- <input type="submit" class="btn btn-custom"> --}}
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection
