@extends('user.layout.main')
@section('content')
    <div class="container">
        <div class="judul-pesanan mt-5">
            <h3 class="text-center font-weight-bold">RIWAYAT PESANAN</h3>
            <br>
        </div>
        <table class="table table-bordered" id="example">
            <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Produk</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Subharga</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transaksi_details as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->produk->nama_produk }}</td>
                        <td>@currency($d->produk->harga)</td>
                        <td>{{ $d->qty }}</td>
                        <td>@currency($d->subtotal)</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="4">Total Belanja</th>
                    <th colspan="1">@currency($totalBelanja)</th>
                </tr>
            </tfoot>
        </table><br>
        <center>
            <p>Scan ini di kasir untuk melanjutkan pembayaran</p>

            <img src="{{ asset('img/qr.png') }}" alt="QR Code untuk pembayaran" width="200">
        </center>


    </div>
@endsection
