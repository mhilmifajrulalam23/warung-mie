@extends('user.layout.main')
@section('content')
    <div class="container">
        <div class="judul-pesanan mt-5">
            <h3 class="text-center font-weight-bold">RIWAYAT PESANAN</h3>
            <br>
        </div>
        @if ($transaksi->isEmpty())
            <table class="table table-bordered" id="example">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tanggal Pesanan</th>
                        <th scope="col">Total Bayar</th>
                        <th scope="col">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="6"><center>Anda belum memiliki pesanan</center></td>
                    </tr>
                </tbody>
            </table>
        @else
        <table class="table table-bordered" id="example">
            <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Tanggal Pesanan</th>
                    <th scope="col">Total Bayar</th>
                    <th scope="col">Opsi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transaksi as $t)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $t->created_at->format('d-m-Y') }}</td>
                        <td>@currency($t->total_belanja)</td>
                        <td>
                            <a href="{{ route('detail', ['id' => $t->id]) }}" class="btn btn-primary">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
@endsection
