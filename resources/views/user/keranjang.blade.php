@extends('user.layout.main')

@section('content')
    <div class="container">
        <div class="judul-pesanan mt-5">
            <h3 class="text-center font-weight-bold">PESANAN ANDA</h3>
            <br>
        </div>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @if ($keranjangs->isEmpty())
        <table class="table table-bordered" id="example">
            <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Pesanan</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Subharga</th>
                    <th scope="col">Opsi</th>
                </tr>
            </thead>
            <tbody>
               <tr>
                <td colspan="6"><center>Keranjang Anda kosong</center></td>
               </tr>
            </tbody>
        </table>

        {{-- <center><p>Keranjang Anda kosong.</p></center> --}}
        @else
            <table class="table table-bordered" id="example">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Pesanan</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Jumlah</th>
                        <th scope="col">Subharga</th>
                        <th scope="col">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($keranjangs as $k)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $k->produk->nama_produk }}</td>
                            <td>@currency($k->produk->harga)</td>
                            <td>{{ $k->qty }}
                            </td>
                            <td>@currency($k->subtotal)</td>
                            <td>
                                <form action="{{ route('keranjang.destroy', $k->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4">Total Belanja</th>
                        <th colspan="2">@currency($totalBelanja)</th>
                    </tr>
                </tfoot>
            </table><br>
            <form method="POST" action="{{ route('konfirmasi') }}">
                @csrf
                <a href="{{ route('menu') }}" class="btn btn-primary btn-sm">Lihat Menu</a>
                <button type="submit" class="btn btn-success btn-sm">Konfirmasi Pesanan</button>
            </form>
        @endif
    </div>



@endsection
