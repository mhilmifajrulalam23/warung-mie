@extends('user.layout.main')
@section('content')
    <section class="home" id="home">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="home-content" data-aos="fade-up" data-aos-duration="1000">
                        <h1 class="text-home-bold fw-bold text-dark mt-1">
                            Mie Nikmat, <br><span class="text-primary">Harga Hemat</span>
                        </h1>
                        <h4 class="text-home-reguler fw-normal text-secondary">
                            Rasakan kelezatan mie dengan cita rasa autentik tanpa menguras dompet.
                            Kami menyajikan berbagai pilihan mie yang dibuat dari bahan-bahan segar dan berkualitas.
                        </h4>
                        <div class="home-btn mt-5">
                            <a href="{{route('menu')}}" class="btn btn-custom">Order now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="home-img" data-aos="fade-up" data-aos-duration="2000">
                        <img src="{{asset('img/mie.png')}}" class="w-100" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
