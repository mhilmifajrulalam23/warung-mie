<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('gambar/logo.png') }}">
    <title>Beranda</title>
    <link rel="icon" type="image/x-icon" href="{{asset('img/title.png')}}">
    {{-- bootstrap --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    {{-- fontawesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    {{-- google font --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700&display=swap" rel="stylesheet">
    {{-- css --}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>
    <!--navbar-->
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('img/logo.png') }}" alt="Bootstrap" width="80" height="24">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('menu')}}">Menu</a>
                    </li>
                </ul>
            </div>
            <a href="{{ route('login') }}" class="btn btn-custom">Login</a>
        </div>
    </nav>
    <!--end navbar-->

    <!--content-->
    <section class="home" id="home">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="home-content" data-aos="fade-up" data-aos-duration="1000">
                        <h1 class="text-home-bold fw-bold text-dark mt-1">
                            Mie Nikmat, <br><span class="text-primary">Harga Hemat</span>
                        </h1>
                        <h4 class="text-home-reguler fw-normal text-secondary">
                            Rasakan kelezatan mie dengan cita rasa autentik tanpa menguras dompet.
                            Kami menyajikan berbagai pilihan mie yang dibuat dari bahan-bahan segar dan berkualitas.
                        </h4>
                        <div class="home-btn mt-5">
                            <a href="{{route('menu')}}" class="btn btn-custom">Order now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="home-img" data-aos="fade-up" data-aos-duration="2000">
                        <img src="{{ asset('img/mie.png') }}" class="w-100" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--footer-->
    <div class="container">
        <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <div class="col-md-4 d-flex align-items-center">
                <a href="https://getbootstrap.com/"
                    class="mb-3 me-2 mb-md-0 text-body-secondary text-decoration-none lh-1">
                    <img src="{{ asset('img/logo.png') }}" alt="gambar" class="bi" width="80" height="24">
                </a>
                <span class="mb-3 mb-md-0 text-body-secondary">© 2024 M. Hilmi Fajrul Alam</span>
            </div>

            <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
                <a href="" class="text-body-secondary ms-3"><i class="fa-brands fa-facebook-f"></i></a>
                <a href="" class="text-body-secondary ms-3"><i class="fa-brands fa-twitter"></i></a>
                <a href="" class="text-body-secondary ms-3"><i class="fa-brands fa-instagram"></i></a>
            </ul>
        </footer>
    </div>
    <!--end footer-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous">
    </script>
</body>

</html>
