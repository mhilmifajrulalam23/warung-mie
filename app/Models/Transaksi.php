<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_users',
        'total_belanja'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_users');
    }
    public function transaksi_details()
    {
        return $this->hasMany(Transaksi_Detail::class, 'id_transaksis');
    }
}
