<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Transaksi;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
        $totalUser = User::count();
        $totalProduk = Produk::count();
        $totalTransaksi = Transaksi::count();
        $totalUangMasuk = Transaksi::sum('total_belanja');

        return view('admin.dashboard', compact('totalUser', 'totalProduk', 'totalTransaksi', 'totalUangMasuk'));
    }
}
