<?php

namespace App\Http\Controllers;

use App\Models\Keranjang;
use App\Models\Transaksi;
use App\Models\Transaksi_Detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransaksiController extends Controller
{
    public function index(){
        $transaksi = Transaksi::with('user')->get();
        return view('admin.tabel-pesanan', compact('transaksi'));
    }

    public function store(Request $request)
    {
        // Ambil data keranjang untuk proses transaksi
        $keranjangs = Keranjang::where('id_users', Auth::user()->id)->get();

        // Menghitung total belanja
        $totalBelanja = $keranjangs->sum(function($keranjang) {
            return $keranjang->produk->harga * $keranjang->qty;
        });

        // Simpan transaksi baru dengan total_belanja
        $transaksi = Transaksi::create([
            'id_users' => Auth::user()->id,
            'total_belanja' => $totalBelanja,
        ]);

        foreach ($keranjangs as $keranjang) {
            // Simpan detail transaksi
            Transaksi_Detail::create([
                'id_transaksis' => $transaksi->id,
                'id_produks' => $keranjang->id_produks,
                'qty' => $keranjang->qty,
            ]);

            // Hapus item dari keranjang setelah transaksi berhasil
            $keranjang->delete();
        }

        return redirect()->route('pesanan')->with('success', 'Transaksi berhasil dilakukan.');
    }

    public function delete($id)
    {
        $transaksi = Transaksi::findOrFail($id);

        // Hapus detail transaksi yang terkait
        $transaksi->transaksi_details()->delete();

        // Hapus transaksi
        $transaksi->delete();

        return redirect()->route('pesanan.index')->with('success', 'Data transaksi berhasil dihapus.');
    }

    public function show($id){
    $transaksi = Transaksi::findOrFail($id);
    $transaksi_details = Transaksi_Detail::where('id_transaksis', $id)->get();

    return view('admin.tabel-detail', compact('transaksi', 'transaksi_details'));
}

}
