<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use App\Models\Transaksi_Detail;
use Illuminate\Http\Request;

class TransaksiDetailController extends Controller
{
    public function index(){

        $transaksi_detail = transaksi_detail::all();
        return view('admin.tabel-detail', compact('transaksi_detail'));
    }
    public function show($id){
        $transaksi = transaksi::findOrFail($id);
        $transaksi_details = Transaksi_Detail::where('id_transaksis', $id)->get();


        // Inisialisasi subtotal untuk setiap transaksi detail
        foreach ($transaksi_details as $detail) {
            $detail->subtotal = $detail->produk->harga * $detail->qty;
        }

        // Menghitung total belanja keseluruhan
        $totalBelanja = $transaksi_details->sum('subtotal');

        return view('user.detail', compact('transaksi', 'transaksi_details', 'totalBelanja'));
    }
}
