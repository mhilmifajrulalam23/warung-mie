<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ProdukController extends Controller
{
    public function index(){

        $produk = Produk::all();

        return view('admin.tabel-produk',compact('produk'));
    }

    public function create(){

        return view('admin.tambah-produk');

    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'nama_produk' => 'required',
            'deskripsi' => 'required',
            'jenis' => 'required',
            'harga' => 'required',
            'gambar' => 'required|mimes:png,jpg,jpeg',
        ]);

        if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $gambar                   = $request->file('gambar');
        $filename                 = date('Y-m-d').$gambar->getClientOriginalName();
        $path                     ='gambar-produk/'.$filename;

        Storage::disk('public')->put($path,file_get_contents($gambar));

        $produk['nama_produk']    = $request->nama_produk;
        $produk['deskripsi']      = $request->deskripsi;
        $produk['jenis']          = $request->jenis;
        $produk['harga']          = $request->harga;
        $produk['gambar']         = $filename;

        Produk::create($produk);

        return redirect()->route('produk.index');
    }
    public function edit(Request $request, $id){

        $produk = Produk::find($id);

        return view('admin.edit-produk', compact('produk'));
    }

    public function update(Request $request,$id){
        $validator = validator::make($request->all(),[
            'nama_produk' => 'required',
            'deskripsi' => 'required',
            'jenis' => 'required',
            'harga' => 'required',
            'gambar' => 'nullable|mimes:png,jpg,jpeg'
        ]);

        if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $find = Produk::find($id);

        $produk['nama_produk']    = $request->nama_produk;
        $produk['deskripsi']      = $request->deskripsi;
        $produk['jenis']          = $request->jenis;
        $produk['harga']          = $request->harga;

        $gambar                   = $request->file('gambar');

        if($gambar){
            $filename                 = date('Y-m-d').$gambar->getClientOriginalName();
            $path                     ='gambar-produk/'.$filename;

            if($find->gambar){
                Storage::disk('public')->delete('gambar-produk/'.$find->gambar);
            }

            Storage::disk('public')->put($path,file_get_contents($gambar));
            $produk['gambar']    = $filename;
        }

        $find->update($produk);

        return redirect()->route('produk.index');
    }

    public function delete(Request $request,$id){
        $produk = Produk::find($id);

        if($produk){
            $produk->delete();
        }
        return redirect()->route('produk.index');
    }
}
