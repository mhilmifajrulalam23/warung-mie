<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(){
        return view('user.dashboard');
    }

    public function menu(){
        $produk = Produk::all();

        return view('user.menu', compact('produk'));
    }

    public function keranjang(){
        return view('user.keranjang');
    }

    public function pesanan(){
    $userId = Auth::id();

    // Mengambil data transaksi berdasarkan ID user
    $transaksi = Transaksi::where('id_users', $userId)->get();

    return view('user.pesanan', compact('transaksi'));
    }
}
