<?php

namespace App\Http\Controllers;

use App\Models\Detail;
use App\Models\Produk;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    public function index(){
        $detail =Detail::all();

        return view ('admin.tabel-detail', compact('detail'));
    }

    public function tambah(Request $request, $id){
        $detail['id_pesanan']       = $request->id_pesanan;
        $detail['id_produk']      = $request->id_produk;
        $detail['jumlah']   = $request->jumlah;

        Detail::create($detail);

        return redirect()->route('keranjang');
    }
}
