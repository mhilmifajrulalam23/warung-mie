<?php

namespace App\Http\Controllers;

use App\Models\Keranjang;
use App\Models\Transaksi;
use App\Models\Transaksi_Detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KeranjangController extends Controller
{
    public function index()
    {
        $keranjangs = Keranjang::where('id_users', Auth::id())->with('produk')->get();

        // Menghitung subtotal untuk setiap item keranjang
        foreach ($keranjangs as $keranjang) {
            $subtotal = $keranjang->produk->harga * $keranjang->qty;
            $keranjang->subtotal = $subtotal; // Menambahkan atribut subtotal ke objek $keranjang
        }

        // Menghitung total belanja keseluruhan
        $totalBelanja = $keranjangs->sum('subtotal');

        return view('user.keranjang', compact('keranjangs', 'totalBelanja'));
    }

    public function store(Request $request)
    {
        // Cek apakah produk sudah ada di keranjang user
        $cekproduk = Keranjang::where('id_users', Auth::id())->where('id_produks', $request->id_produks)->first();

        if ($cekproduk) {
            // Jika sudah ada, tambahkan qty baru ke qty yang sudah ada
            $cekproduk->qty += 1;
            $cekproduk->save();

            return redirect()->route('keranjang')->with('success', 'Berhasil menambahkan produk ke keranjang.');
        }

        // Jika belum ada, tambahkan produk baru ke keranjang
        Keranjang::create([
            'id_users' => Auth::id(),
            'id_produks' => $request->id_produks,
            'qty' => 1
        ]);

        return redirect()->route('keranjang')->with('success', 'Berhasil menambahkan produk ke keranjang.');
    }

    // public function update(Request $request, $id)
    // {
    //     $keranjang = Keranjang::findOrFail($id);
    //     $keranjang->update([
    //         'qty' => $request->qty
    //     ]);

    //     return redirect()->back()->with('success', 'Berhasil memperbarui keranjang.');
    // }

    public function destroy($id)
    {
        $keranjang = Keranjang::findOrFail($id);
        $keranjang->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus item dari keranjang.');
    }
}
