<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class LoginController extends Controller
{
    public function index(){
        return view('login');
    }

    public function login(Request $request){
        $validator = Validator::make ($request->all(),[
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $data = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if(Auth::attempt($data)){
            if(Auth::user()->role == 'admin'){
                return redirect()->route('dashboard.admin');
            }elseif(Auth::user()->role == 'user'){
                return redirect()->route('dashboard.user');
            }
        }else{
            return redirect()->route('login')->with('failed','Email dan Password tidak sesuai')->withInput();
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('homepage')->with('succes','kamu berhasil logout');
    }

    public function daftar(){
        return view('register');
    }

    public function daftar_proses(Request $request){
        $request->validate([
            'nama'  => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
        ]);

        $data['nama']       = $request->nama;
        $data['email']      = $request->email;
        $data['password']   = Hash::make($request->password) ;

        User::create($data);

        $daftar = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if(Auth::attempt($daftar)){
            if(Auth::user()->role == 'admin'){
                return redirect()->route('dashboard.admin');
            }elseif(Auth::user()->role == 'user'){
                return redirect()->route('dashboard.user');
            }
        }else{
            return redirect()->route('login')->with('failed','Email dan Password tidak sesuai')->withInput();
        }
    }
}
