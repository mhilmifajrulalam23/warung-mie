<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;


class UserManagementController extends Controller
{
    public function index(){

        // $data_user = User::get();
        $user = User::all();

        return view ('admin.tabel-user', compact('user'));
    }

    public function create(){

        return view('admin.tambah-user');

    }

    public function store(Request $request){
        $validator = validator::make($request->all(),[
            'nama' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'role'     => 'required',
        ]);

        if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
        $user['nama']       = $request->nama;
        $user['email']      = $request->email;
        $user['password']   = Hash::make($request->password) ;
        $user['role']       = $request->role;

        User::create($user);

        return redirect()->route('user.index');
    }

    public function edit(Request $request, $id){

        $user = User::find($id);

        return view('admin.edit-user', compact('user'));
    }

    public function update(Request $request,$id){
        $validator = validator::make($request->all(),[
            'nama' => 'required',
            'email' => 'required|email',
            'password' => 'nullable',
            'role' => 'nullable',
        ]);

        if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $user['nama']       = $request->nama;
        $user['email']      = $request->email;
        $user['password']   = Hash::make($request->password) ;
        $user['role']       = $request->role;

        if($request->password){
            $user['password'] = Hash::make($request->password) ;
        }

        if($request->role){
            $user['role'] = $request->role;
        }

        User::whereId($id)->update($user);

        return redirect()->route('user.index');
    }

    public function delete(Request $request,$id){
        $user = User::find($id);

        if($user){
            $user->delete();
        }
        return redirect()->route('user.index');
    }
}
